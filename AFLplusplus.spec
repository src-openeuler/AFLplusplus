Name:		AFLplusplus
Version:	4.10c
Summary:	Practical, instrumentation-driven fuzzer for binary formats
License:	Apache 2.0
Release:	4
URL:		https://github.com/AFLplusplus/AFLplusplus
Source0:	https://github.com/AFLplusplus/AFLplusplus/archive/refs/tags/v%{version}.tar.gz

BuildRequires: gcc
BuildRequires: gcc-plugin-devel
BuildRequires: clang
BuildRequires: llvm-devel
BuildRequires: make chrpath
Requires: gcc gcc-c++

%global afl_helper_path %{_libdir}/afl
%define _lto_cflags %{nil}

%description
American fuzzy lop uses a novel type of compile-time instrumentation
and genetic algorithms to automatically discover clean, interesting
test cases that trigger new internal states in the targeted
binary. This substantially improves the functional coverage for the
fuzzed code. The compact synthesized corpuses produced by the tool are
also useful for seeding other, more labor- or resource-intensive
testing regimes down the road.

%package clang

Summary: Clang and clang++ support for AFLplusplus
%ifarch x86_64
BuildRequires: lld
%endif
Requires: clang

%description clang
This subpackage contains clang and clang++ support for AFLplusplus

%prep
%forgeautosetup -p1

%build
%ifnarch x86_64
AFL_NO_X86=1 \
%endif
CFLAGS="%{optflags}" \
%{__make} %{?_smp_mflags} \
  PREFIX="%{_prefix}" \
  HELPER_PATH="%{afl_helper_path}" \
  DOC_PATH="%{_pkgdocdir}" \
  MAN_PATH="%{_mandir}/man8" \
  MISC_PATH="%{_pkgdocdir}" \
  source-only

%install
%ifnarch x86_64
AFL_NO_X86=1 \
%endif
%{make_install} \
  PREFIX="%{_prefix}" \
  HELPER_PATH="%{afl_helper_path}" \
  DOC_PATH="%{_pkgdocdir}" \
  MAN_PATH="%{_mandir}/man8" \
  MISC_PATH="%{_pkgdocdir}"

%ifnarch x86_64
# On non-x86 these files are built and installed but they don't
# function, so delete them.  Only afl-clang-fast* works.
# afl-clang-fast* is a symlink to afl-cc / afl-c++ so we cannot delete
# those binaries.
rm $RPM_BUILD_ROOT%{_bindir}/afl-clang
rm $RPM_BUILD_ROOT%{_bindir}/afl-clang++
rm $RPM_BUILD_ROOT%{_bindir}/afl-gcc
rm $RPM_BUILD_ROOT%{_bindir}/afl-g++
rm $RPM_BUILD_ROOT%{_mandir}/man8/afl-cc.8*
rm $RPM_BUILD_ROOT%{_mandir}/man8/afl-c++.8*
%endif

# ERROR: No build ID note found in <.o file>
chmod -x $RPM_BUILD_ROOT%{afl_helper_path}/*.o

# Remove docs since we will package them using %%doc.
mv $RPM_BUILD_ROOT%{_pkgdocdir} pkg-docs

%files
%license docs/COPYING
%doc pkg-docs/*
%ifarch x86_64
%{_bindir}/afl-g++
%{_bindir}/afl-gcc
%endif
%{_bindir}/afl-analyze
%{_bindir}/afl-addseeds
%{_bindir}/afl-cc
%{_bindir}/afl-c++
%{_bindir}/afl-cmin
%{_bindir}/afl-cmin.bash
%{_bindir}/afl-fuzz
%{_bindir}/afl-gcc-fast
%{_bindir}/afl-g++-fast
%{_bindir}/afl-gotcpu
%{_bindir}/afl-persistent-config
%{_bindir}/afl-plot
%{_bindir}/afl-showmap
%{_bindir}/afl-system-config
%{_bindir}/afl-tmin
%{_bindir}/afl-whatsup
%dir %{afl_helper_path}
%{afl_helper_path}/afl-as
%{afl_helper_path}/as
%if 0%{?__isa_bits} == 32
%{afl_helper_path}/afl-compiler-rt-32.o
%else
%{afl_helper_path}/afl-compiler-rt-64.o
%endif
# LoongArch64 clang compiler can compile afl-compiler-rt-32.o on 64_bit platform. 
# For details, see compile log.
%ifarch loongarch64
%exclude %{afl_helper_path}/afl-compiler-rt-32.o
%endif
%{afl_helper_path}/afl-compiler-rt.o
%{afl_helper_path}/afl-gcc-cmplog-pass.so
%{afl_helper_path}/afl-gcc-cmptrs-pass.so
%{afl_helper_path}/afl-gcc-pass.so
%{afl_helper_path}/afl-gcc-rt.o
%{afl_helper_path}/injection-pass.so
%ifarch x86_64
%{_mandir}/man8/afl-c++.8*
%{_mandir}/man8/afl-cc.8*
%endif
%{_mandir}/man8/afl-addseeds.8*
%{_mandir}/man8/afl-analyze.8*
%{_mandir}/man8/afl-as.8*
%{_mandir}/man8/afl-cmin.8*
%{_mandir}/man8/afl-cmin.bash.8*
%{_mandir}/man8/afl-fuzz.8*
%{_mandir}/man8/afl-gcc-fast.8*
%{_mandir}/man8/afl-g++-fast.8*
%{_mandir}/man8/afl-gotcpu.8*
%{_mandir}/man8/afl-plot.8*
%{_mandir}/man8/afl-persistent-config.8*
%{_mandir}/man8/afl-showmap.8*
%{_mandir}/man8/afl-system-config.8*
%{_mandir}/man8/afl-tmin.8*
%{_mandir}/man8/afl-whatsup.8*


%files clang
%license docs/COPYING

%ifarch x86_64
%{_bindir}/afl-clang
%{_bindir}/afl-clang++
%endif
%{_bindir}/afl-clang-fast
%{_bindir}/afl-clang-fast++
%ifarch x86_64
%{_bindir}/afl-clang-lto
%{_bindir}/afl-clang-lto++
%{_bindir}/afl-ld-lto
%{_bindir}/afl-lto
%{_bindir}/afl-lto++
%endif

%{afl_helper_path}/afl-llvm-dict2file.so
%ifarch x86_64
%{afl_helper_path}/afl-llvm-lto-instrumentlist.so
%endif
%{afl_helper_path}/afl-llvm-pass.so

%ifarch x86_64
%if 0%{?__isa_bits} == 32
%{afl_helper_path}/afl-llvm-rt-lto-32.o
%else
%{afl_helper_path}/afl-llvm-rt-lto-64.o
%endif
%{afl_helper_path}/afl-llvm-rt-lto.o
%endif

%{afl_helper_path}/cmplog-instructions-pass.so
%{afl_helper_path}/cmplog-routines-pass.so
%{afl_helper_path}/cmplog-switches-pass.so
%{afl_helper_path}/compare-transform-pass.so
%{afl_helper_path}/dynamic_list.txt
%{afl_helper_path}/libAFLDriver.a*
%{afl_helper_path}/libAFLQemuDriver.a
%{afl_helper_path}/libdislocator.so
%{afl_helper_path}/libtokencap.so
%ifarch x86_64
%{afl_helper_path}/SanitizerCoverageLTO.so
%endif
%{afl_helper_path}/SanitizerCoveragePCGUARD.so
%{afl_helper_path}/split-compares-pass.so
%{afl_helper_path}/split-switches-pass.so

%{_mandir}/man8/afl-clang-fast.8*
%{_mandir}/man8/afl-clang-fast++.8*
%ifarch x86_64
%{_mandir}/man8/afl-clang-lto.8.gz
%{_mandir}/man8/afl-clang-lto++.8.gz
%{_mandir}/man8/afl-lto.8.gz
%{_mandir}/man8/afl-lto++.8.gz
%endif


%changelog
* Fri Feb 7 2025 liyancheng <412998149@qq.com> - 4.10c-4
- Type: Bugfix
- ID: NA
- SUG: NA
- DESC: Fix build error with lto.

* Wed May 29 2024 yueyuankun <yueyuankun@kylinos.cn> - 4.10c-3
- Type: compile
- ID: NA
- SUG: NA
- DESC: Fix build error on loongarch64

* Thu Apr 25 2024 liyancheng <412998149@qq.com> - 4.10c-2
- Type: Update
- ID: NA
- SUG: NA
- DESC: Update the link to the upstream compressed package.

* Sat Apr 13 2024 liyancheng <412998149@qq.com> - 4.10c-1
- Type: Init
- ID: NA
- SUG: NA
- DESC: Init AFLplusplus repo.
